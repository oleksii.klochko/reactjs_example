# reactjs_example

1)
nano /etc/sudoers

	gitlab-runner ALL=NOPASSWD:/usr/sbin/service nginx reload
	%gitlab-runner ALL=(ALL) NOPASSWD:ALL

2)
install nginx + gitlab-runner + certbot

3)
register gitlab-runner

4)
app_nginx.conf:

	server {

	server_name name.server;

	root /var/www/root-dir/build;
    index index.html;

	listen [::]:443 ssl;
	listen 443 ssl;
	ssl_certificate /etc/letsencrypt/live/name.server/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/name.server/privkey.pem;


	include acme;

	location / {
		try_files $uri /index.html;
	}
	

    location /api {
	proxy_pass http://name.server2/api;
	}

	location /auth {
	proxy_pass http://name.server/api;
	}
 
	}


	server {

    if ($host = name.server) {
	return 301 https://$host$request_uri;
    }

	server_name name.server;

	
	listen 80;
	listen [::]:80;

	return 404;
	include acme;
	}

5)
add ssh of gitlab-ruuner user from remote host to source host
